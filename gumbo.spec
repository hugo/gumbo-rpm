Name:           gumbo-parser
Version:        0.10.1
Release:        0
Summary:        An HTML5 parsing library in pure C99
Group:          Development/Libraries/HTML
License:        Apache-2.0
URL:            https://github.com/google/gumbo-parser
Vendor:         Google.com
Source:         %{url}/archive/v%{version}.tar.gz#/gumbo-parser-%{version}.tar.gz
Prefix:         %{_prefix}
Packager:       hugo <hugo@lysator.liu.se>
BuildRoot:      %{tmppath}/%{name}-root
BuildRequires:  libtool gcc gcc-c++ make

%description
Gumbo is an implementation of the HTML5 parsing algorithm implemented
as a pure C99 library with no outside dependencies. It's designed to
serve as a building block for other tools and libraries such as
linters, validators, templating languages, and refactoring and
analysis tools.

%prep
%setup

%build
./autogen.sh
CFLAGS="$RPM_OPT_FLAGS" ./configure \
	--prefix=%{_prefix} \
	--mandir=%{_mandir} \
	--libdir=%{_libdir} \
	--sysconfdir=%{_sysconfdir} \
	--disable-dependency-tracking # needed since always first run

make -j

%install

# [ "$RPM_BUILD_ROOT" != "/" ] && rm -rf $RPM_BUILD_ROOT

make install DESTDIR=$RPM_BUILD_ROOT

%clean
[ "$RPM_BUILD_ROOT" != "/" ] && rm -rf $RPM_BUILD_ROOT

%files
%{_prefix}/include/gumbo.h
%{_prefix}/include/tag_enum.h
%{_prefix}/%{_lib}/libgumbo*
%{_prefix}/%{_lib}/pkgconfig/gumbo.pc
